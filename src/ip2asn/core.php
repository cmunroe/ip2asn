<?php

namespace ip2asn;

/**
 * Core class functions shared by multiple subclasses.
 */
class core {
    
    private $baseURL = 'https://ip2asn.ipinfo.app/lookup/';
    protected $data = false;

    /**
     * Get the data from the API.
     *
     * @param string $ip address we wish to compare against.
     * @return this chain.
     */
    public function get(string $ip){

        // setup our request url
        $url = $this->baseURL . $ip;

        // Get our file.
        $json = file_get_contents($url);

        // Decode our file.
        $array = json_decode($json, true);

        // Check to see if we got good data.
        if($array['error'] != null){

            // return false as we have nothing.
            // Should break chaining.
            $this->data = false;
            return $this;

        };

        // return our data.
        $this->data = $array;

        return $this;

    }

    /**
     * Return raw data.
     *
     * @return array data
     */
    public function dump(){

        return $this->data;

    }


    /**
     * Return an array of all ASNs annoucing the IP you queried.
     *
     * @return array ASNs.
     */
    public function ASNs(){

        $asn = array();

        if($this->data === false){

            return false;

        }

        foreach($this->data['announcedBy'] as $annoucements){

            $asn[] = $annoucements['asn'];

        }
        
        return $asn;

    }

    /**
     * Change the baseURL used by the class. 
     * 
     * Vital for using standalone's instead of going to the main site.
     *
     * @param string $url
     * @return this.
     */
    public function cache_url(string $url){

        $this->baseURL = $url;

        return $this;

    }
    


}
