<?php

namespace ip2asn;

/**
 * Lookup Data about yourself, and your own IPs.
 */
class me extends core {

    /**
     * Get your IP address Info.
     *
     * @return this chain.
     */
    public function getMe(){

        $this->get("@me");

        return $this;

    }

    /**
     * Return your current IP Address.
     *
     * @return string your ip address.
     */
    public function getMyIP(){

        $this->getMe();

        return $this->data['ipAddress'];

    }


    /**
     * Return your ASNs that are being annouced.
     *
     * @return array asns.
     */
    public function getMyASN(){

        $this->getMe();

        return $this->ASNs();

    }



}